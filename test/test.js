const {checkAge} = require('../src/util.js');
const {checkFullName} = require('../src/util.js');
const {assert, expect} = require('chai');

/*Age: 
	- check if it is a integer value
	- check if it is NOT empty
	- check if it is NOT undefined
	- check if it is NOT null
	- check if it is NOT equal to 0
*/

describe('test_checkAge', () => {

	

	it('age_not_empty', () => {
		const age = "";
		assert.isNotEmpty(checkAge(age));
	})

	it('age_not_undefined', () => {
		const age = undefined;
		assert.isDefined(checkAge(age));
	})

	it('age_not_null', () => {
		const age = null;
		assert.isNotNull(checkAge(age));
	})

	it('age_value_not_zero', () => {
		let age = 0;
		let benchmark = 1;
		assert.isAtLeast(checkAge(age),parseInt(benchmark));
	})

	it('age_type_is_integer', () => {
		const age = 12;
		// console.log(typeof(age))

		// assert.isNumber(checkAge(age));
		expect(checkAge(age)).to.be.a('number')
	})

});

/*Fullname:
		- check if it is NOT empty
		- check if it is NOT null
		- check if it is NOT undefined
		- check if it is a string data type
		- check if the character is NOT equal to 0 // expected failing is similar to check if NOT empty
*/

describe('test_checkFullName', () => {

	
	it('fullName_not_empty', () => {
		const fullName = "";
		assert.isNotEmpty(checkFullName(fullName));

	})

	it('fullName_not_undefined', () => {
		const fullName = undefined;
		assert.isDefined(checkFullName(fullName));
	})

	it('fullName_not_null', () => {
		const fullName = null;
		assert.isNotNull(checkFullName(fullName));
	})

	it('fullName_is_type_string', () => {
			const fullName = 1;
			assert.isString(checkFullName(fullName));
		})

	it('fullName_length_not_zero', () => {
			const fullName = '';
			assert.isNotEmpty(checkFullName(fullName.length));
		})

});