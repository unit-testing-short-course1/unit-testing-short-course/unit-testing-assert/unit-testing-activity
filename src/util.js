
//Age
function checkAge(age) {
	console.log(typeof(age))
	

	if (age === "") {
		return "Error: Age should not be empty!"
	}

	if (age === undefined) {
		return "Error: Age should should not be undefined!"
	}

	if (age === null) {
		return "Error: Age should not be null!"
	}

	console.log(typeof(age))
	if (age <= 0) {
		return 1
	}

	if (typeof age !== 'number') {
			return "Error: age should be a number"
		}
	return age;
}

//Fullname

function checkFullName(fullName) {

	if (fullName === "") {
		return "Error: Name should not be empty!"
	}

	if (fullName === undefined) {
		return "Error: Name should not be undefined!"
	}

	if (fullName === null) {
		return "Error: Name should not be null!"
	}

	if (typeof(fullName) !== 'string') {
		return "Error: Name should be string!"
	}

	// if (fullName.length == 0) {
	// 	return "Error: Name length should not be 0!"
	// }

	return fullName;
}


module.exports = {
	checkAge,
	checkFullName
}